#include<stdio.h>
int input()
{
	float a;
	printf("enter the radius \n");
	scanf("%f",&a);
	return a;
}
int compute(float r)
{
	float area;
	area=3.14*r*r;
	return area;
}
void display(float area)
{
	printf("area=%f",area);
}
int main()
{
	float r,area;
	r=input();
	area=compute(r);
	display(area);
	return 0;
}
